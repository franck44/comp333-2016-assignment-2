package test;

import static org.junit.Assert.*;
import main.Board;
import main.MyBoard;

import org.junit.Test;

public class TestLegal {
	@Test
	public void LinearBoard_3() {
		Board b = new MyBoard(3,"");
		assertFalse(b.hasLegalJumps(new int[]{1,1,1}));
		assertFalse(b.hasLegalJumps(new int[]{1,0,1}));
		assertFalse(b.hasLegalJumps(new int[]{1,0,0}));
		assertFalse(b.hasLegalJumps(new int[]{0,1,0}));
		assertFalse(b.hasLegalJumps(new int[]{0,0,1}));
		assertFalse(b.hasLegalJumps(new int[]{0,0,0}));
		assertTrue(b.hasLegalJumps(new int[]{1,1,0}));
		assertTrue(b.hasLegalJumps(new int[]{0,1,1}));
	}
	
	@Test
	public void LinearBoard_4() {
		Board b = new MyBoard(4,"");
		assertFalse(b.hasLegalJumps(new int[]{1,1,1,1}));
		assertTrue(b.hasLegalJumps(new int[]{1,0,1,1}));
		assertFalse(b.hasLegalJumps(new int[]{1,0,0,1}));
		assertFalse(b.hasLegalJumps(new int[]{0,1,0,1}));
		assertTrue(b.hasLegalJumps(new int[]{0,0,1,1}));
		assertFalse(b.hasLegalJumps(new int[]{0,0,0,1}));
		assertTrue(b.hasLegalJumps(new int[]{1,1,0,1}));
		assertTrue(b.hasLegalJumps(new int[]{0,1,1,1}));
		
		assertTrue(b.hasLegalJumps(new int[]{1,1,1,0}));
		assertFalse(b.hasLegalJumps(new int[]{1,0,1,0}));
		assertFalse(b.hasLegalJumps(new int[]{1,0,0,0}));
		assertFalse(b.hasLegalJumps(new int[]{0,1,0,0}));
		assertFalse(b.hasLegalJumps(new int[]{0,0,1,0}));
		assertFalse(b.hasLegalJumps(new int[]{0,0,0,0}));
		assertTrue(b.hasLegalJumps(new int[]{1,1,0,0}));
		assertTrue(b.hasLegalJumps(new int[]{0,1,1,0}));
	}
}
