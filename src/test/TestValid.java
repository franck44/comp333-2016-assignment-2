package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import main.Board;
import main.MyBoard;

import org.junit.Test;

public class TestValid {
	@Test
	public void LinearBoard_3() {
		Board b = new MyBoard(3,"");
		assertFalse(b.isValidJump(0,1,2, new int[]{1,1,1}));
		assertTrue(b.isValidJump(0,1,2, new int[]{1,1,0}));
		assertFalse(b.isValidJump(2,1,0, new int[]{1,1,0}));
		assertTrue(b.isValidJump(2,1,0, new int[]{0,1,1}));
	}
	
	@Test
	public void LinearBoard_4() {
		Board b = new MyBoard(4,"");
		assertFalse(b.isValidJump(0,1,2, new int[]{1,1,1,0}));
		assertTrue(b.isValidJump(0,1,2, new int[]{1,1,0,0}));
		assertFalse(b.isValidJump(2,1,0, new int[]{1,1,0,0}));
		assertTrue(b.isValidJump(2,1,0, new int[]{0,1,1,0}));
		assertTrue(b.isValidJump(1,2,3, new int[]{0,1,1,0}));
	}
}
