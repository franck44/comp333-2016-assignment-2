package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import main.Board;
import main.MyBoard;

import org.junit.Test;

public class TestSolvable {
	@Test
	public void LinearBoard_3() {
		Board b = new MyBoard(3,"");
		assertFalse(b.isSolvable(new int[]{1,1,1}));
		assertTrue(b.isSolvable(new int[]{1,1,0}));
		assertTrue(b.isSolvable(new int[]{1,0,0}));
		assertFalse(b.isSolvable(new int[]{0,0,0}));
	}
	
	@Test
	public void LinearBoard_4() {
		Board b = new MyBoard(4,"");
		assertTrue(b.isSolvable(new int[]{1,1,0,1}));
		assertFalse(b.isSolvable(new int[]{1,1,1,0}));
	}
	
	@Test
	public void LinearBoard_6() {
		Board b = new MyBoard(6,"");
		assertTrue(b.isSolvable(new int[]{1,0,1,1,1,1}));
		assertFalse(b.isSolvable(new int[]{1,1,0,1,1,1}));
	}
}
