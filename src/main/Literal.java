package main;

/**
 * This class implements a literal as a subclass of Vertex. A literal has 
 * a name, a negation property which is set to true if the name starts with 
 * '-' and a variable name var. These fields can be accessed publicly so 
 * you can add any access modifier if you require one.
 * 
 * @author Tahiry Rabehaja
 *
 */

public class Literal extends Vertex{
	public final static Literal FALSE = new Literal("FALSE");
	public final static Literal TRUE = new Literal("TRUE");
	public String name;
	public boolean isNegated = false;
	public String var;
	
	// this will be unused
	public Literal(Integer info) {
		super(info);
	}
	
	public Literal(String name) {
		super(0);
		this.name = name;
		if (name.charAt(0) == '-'){
			isNegated = true;
			var = name.substring(1,name.length());
		} else {
			var = name;
		}
	}
	
	/**
	 * You can use this method to return the negation of a literal.
	 * @param a is a literal.
	 * @return the negation of a as a literal object.
	 */
	public static Literal not(Literal a){
		if (a.isNegated)
			return new Literal(a.var);
		return new Literal("-" + a.var);
	}
	
	public String toString(){
		return this.name;
	}
	
	public boolean equals(Object v){
		return ((Literal)v).name.equals(this.name);
	}
	
	public int hashCode(){
		return name.hashCode();
	}
	
}
