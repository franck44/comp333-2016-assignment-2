package main;

import java.util.Arrays;

public class App {
	public static void main(String[] args) {
		Board b = new MyBoard(6,"b0");
		int[] initConfig = new int[]{1,0,1,1,1,1};
 		System.out.println("Starting config:" + Arrays.toString(initConfig));
		System.out.println("Board solvable:" + b.isSolvable(initConfig));

	}
}