package main;

import java.util.LinkedList;
import java.util.List;

import main.Literal;

public abstract class Board {
	/** This contains auxiliary variables in this list, e.g. selector variables. */
	protected List<Literal> aux = new LinkedList<Literal>();	
	/** This contains the x variables. */
	protected final Literal[] x;
	/** This contains the y variables. */
	protected final Literal[] y;
	/** This contains all legal jumps for this board. This must remain public. */
	public List<int[]> LegalJumps = new LinkedList<int[]>();
	/** This stores the unique name of the board */
	protected final String name;
	/** This stores the size of the board (number of holes) */
	protected int size;
	
	/**
	 * This construct a board object with a unique name.
	 * @param n the number of holes in the board.
	 * @param name unique name of the board. This can only contain letters 
	 * and numbers and must start with a letter.
	 */
	public Board(int n, String name){
		this.name = name;
		size = n;;
		x = new Literal[size];
		y = new Literal[size];
		for(int i=0; i<size; i++){
			x[i] = makeVar("x" + i);
			y[i] = makeVar("y" + i);
		}
		populateLegalJumps();
	}
	
	/**
	 * Use this to create new variables.
	 * @param id is the identification of the variable.
	 * @return a new variable (as a literal) with name 'name+id'.
	 */
	protected Literal makeVar(String id){
		return new Literal(name + id);
	}
	
	/**
	 * Populates LegalJumps with the jumps that are legal according to the 
	 * shape of the board.
	 */
	public abstract void populateLegalJumps();
	
	/**
	 * This checks if a triple is a valid jump from a given configuration.
	 * @param i jumping peg
	 * @param j jumped peg
	 * @param k hole where the jumping peg ends up
	 * @param x_values initial configuration of the board.
	 * @return true iff the jump (i,j,k) is valid from the configuration x_values
	 */
	public abstract boolean isValidJump(int i, int j, int k, int[] x_values);
	
	/**
	 * This constructs a valid jump formula in 2-CNF form. 
	 * @param i jumping peg
	 * @param j jumped peg
	 * @param k hole where the jumping peg ends up
	 * @return a list of pairs of literals. 
	 */
	protected abstract List<Literal[]> valid(int i, int j, int k);
	
	/**
	 * This simplifies a CNF formula when the values of the x variables are known. This 
	 * can be achieved by systematically removing the x variables while preserving the logical truth.
	 * @param formula CNF formula
	 * @param x_values arrays of the values of the x's.
	 * @return a CNF formula where the x's have been logically substituted with the 
	 * respective values.
	 */
	protected abstract List<Literal[]> setX(List<Literal[]> formula, int[] x_values);
	
	/**
	 * This checks if there is at least one legal jump from the configuration x_values
	 * @param x_values initial configuration of the board.
	 * @return true iff there is at least one legal jump from the configuration x_values
	 */
	public abstract boolean hasLegalJumps(int[] x_values);
	
	/**
	 * This constructs a  CNF formula that is satisfiable iff there is a legal jump. 
	 * @return a list of literals that is the CNF representation of a legal board. 
	 */
	protected abstract List<Literal[]> legal();
	
	/**
	 * This checks if the board is solvable ,i.e, there is a sequence of moves
	 * that leads to a configuration with a single peg. 
	 * @param x_values initial configuration of the board.
	 * @return true iff the board is solvable. 
	 */
	public abstract boolean isSolvable(int[] x_values);
	
	/**
	 * This returns all variables used by the latest call that introduced new variables.
	 * @return list of all variables including selector variables.
	 */
	public List<Literal> getAllVars(){
		List<Literal> vars = new LinkedList<Literal>();
		for(Literal l: x)
			vars.add(l);
		for(Literal l: y)
			vars.add(l);
		for(Literal l: aux)
			vars.add(l);
		return vars;
	}
	
}
