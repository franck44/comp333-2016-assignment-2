package main;


/**
 * This vertex class implements the graph vertices mostly used in shortest path problems.
 * A vertex contains a info (used as an identifier) and a double field 'd' which stores the 
 * estimated shortest distance in Dijkstra's and the Bellman-Ford algorithms. 
 */
public class Vertex implements Comparable<Vertex> {
	/* This flag can be used for may traversal algorithm. Just don't forget to reset it to false
	 *  before use */
	public boolean visited = false;
	/* Identifier of the vertex. Two vertices with the same info will be considered the same */
	public Integer info;
	
	/* Shortest path estimation is stored in d */
	public double d;
	
	public  Vertex(Integer info){
		this.info = info;
	}
	
	public String toString(){
		return this.info + "";
	}
	
	/* This is used by priority queue or min-heap to compare vertices based on the value of d */
	public int compareTo(Vertex v) {
		return Double.compare(d, v.d);
	}
	
	public boolean equals(Object v){
		return ((Vertex)v).info == this.info;
	}
	
	public int hashCode(){
		return info.hashCode();
	}
}
