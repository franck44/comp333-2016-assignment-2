package main;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IProblem;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;

public class SATWrapper {
	private Map<Literal,Integer> lit2int = new HashMap<Literal,Integer>();
	
	public boolean solve(List<Literal[]> formula, List<Literal> vars) {
		for (Literal s: vars)
			add_var(s);
		try{
			ISolver solver = SolverFactory.newDefault();
			solver.setExpectedNumberOfClauses(formula.size());
			for (Literal[] clause : formula) {
				  int[] clause_ = toIntArray(clause);
				  solver.addClause(new VecInt(clause_));
			}
			IProblem problem = solver;
			return problem.isSatisfiable();
		} catch(ContradictionException e){
			return false;
		} catch (TimeoutException e) {
			System.err.println(e.getMessage());
			return false;
		} 
	}
	
	private int[] toIntArray(Literal[] clause){
		int[] ret = new int[clause.length];
		for(int i=0; i<clause.length; i++){
			Literal l = clause[i];
			ret[i] = l.isNegated?-lit2int.get(Literal.not(l)):lit2int.get(l);
		}
		return ret;
	}
	
	private void add_var(Literal l){
		Literal not_l = Literal.not(l);
		if (lit2int.containsKey(not_l))
			lit2int.put(l, -lit2int.get(not_l));
		else {
			if (l.isNegated)
				lit2int.put(l, -lit2int.size()+1);
			else
				lit2int.put(l, lit2int.size()+1);
		}
	}
}
