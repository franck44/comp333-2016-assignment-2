#!/bin/bash
#    This is how your programs will be tested

# set up CLASSPATH
export CLASSPATH="$CLASSPATH:lib/*:src/main/:src/test/"

# run all tests
javac -d . src/main/*.java src/test/*.java
java TestRunner
